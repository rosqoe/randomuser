//
//  UserApiResponse.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation

struct UserApiResponse: Decodable {

    // MARK: - Properties

    let results: [Result]

    // MARK: - CodingKeys

    enum OuterKeys: String, CodingKey { case name, location, email, dob, phone, picture }
    enum NameKeys: String, CodingKey { case first, last }
    enum LocationKeys: String, CodingKey { case city }
    enum DobKeys: String, CodingKey { case age }
    enum PictureKeys: String, CodingKey { case imageUrl = "large" }

    // MARK: - Nested structs

    struct Result: Decodable {
        let first: String
        let last: String
        let city: String
        let email: String
        let age: Int
        let phone: String
        let imageUrl: String

        init(from decoder: Decoder) throws {
            let outerContainer = try decoder.container(keyedBy: OuterKeys.self)
            let nameContainer = try outerContainer.nestedContainer(keyedBy: NameKeys.self, forKey: .name)
            let locationContainer = try outerContainer.nestedContainer(keyedBy: LocationKeys.self, forKey: .location)
            let dobContainer = try outerContainer.nestedContainer(keyedBy: DobKeys.self, forKey: .dob)
            let pictureContainer = try outerContainer.nestedContainer(keyedBy: PictureKeys.self, forKey: .picture)
            self.first = try nameContainer.decode(String.self, forKey: .first)
            self.last = try nameContainer.decode(String.self, forKey: .last)
            self.city = try locationContainer.decode(String.self, forKey: .city)
            self.email = try outerContainer.decode(String.self, forKey: .email)
            self.age = try dobContainer.decode(Int.self, forKey: .age)
            self.phone = try outerContainer.decode(String.self, forKey: .phone)
            self.imageUrl = try pictureContainer.decode(String.self, forKey: .imageUrl)
        }
    }

    struct Dob: Decodable {
        let age: Int
    }

    struct Location: Decodable {
        let city: String
    }

    struct Name: Decodable {
        let first, last: String
    }

    struct Picture: Decodable {
        let large: String
    }
}
