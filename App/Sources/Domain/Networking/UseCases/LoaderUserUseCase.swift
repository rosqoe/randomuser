//
//  LoaderUserUseCase.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation

struct LoaderUserUseCase: LoaderUserUseCaseLogic {

    // MARK: - Properties

    private let asyncLoaderWorker: AsyncLoaderWorkerLogic
    private let stringUrl: String = "https://randomuser.me/api/"

    // MARK: - Initializer

    init(asyncLoaderWorker: AsyncLoaderWorkerLogic = AsyncLoaderWorker()) {
        self.asyncLoaderWorker = asyncLoaderWorker
    }

    // MARK: - Actions

    func load() async throws -> UserApiResponse {
        guard let url: URL = .init(string: stringUrl) else {
            throw ApiError.requestFailed(description: "incorrect url")
        }
        return try await asyncLoaderWorker.load(with: .init(url: url))
    }
}
