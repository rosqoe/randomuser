//
//  LoaderUserUseCaseLogic.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation

protocol LoaderUserUseCaseLogic {

    // MARK: - Action

    func load() async throws -> UserApiResponse
}
