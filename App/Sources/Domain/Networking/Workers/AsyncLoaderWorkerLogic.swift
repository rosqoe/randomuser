//
//  AsyncLoaderWorkerLogic.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation

protocol AsyncLoaderWorkerLogic {

    // MARK: - Actions

    func load<T: Decodable>(with request: URLRequest) async throws -> T
}
