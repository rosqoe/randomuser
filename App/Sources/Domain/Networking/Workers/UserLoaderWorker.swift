//
//  AsyncLoaderWorker.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation

struct AsyncLoaderWorker: AsyncLoaderWorkerLogic {

    // MARK: - Properties

    private let session: URLSession
    private let decoder: JSONDecoder = .init()

    // MARK: - Initializer

    init(session: URLSession = .init(configuration: .default)) {
        self.session = session
    }

    // MARK: - Actions

    func load<T: Decodable>(with request: URLRequest) async throws -> T {
        let (data, response) = try await session.data(for: request)
        try responseHandler(with: response)
        do {
            return try decoder.decode(T.self, from: data)
        } catch {
            throw ApiError.jsonConversionFailure(description: error.localizedDescription)
        }
    }

    // MARK: - Helpers

    private func responseHandler(with response: URLResponse) throws {
        guard let response = response as? HTTPURLResponse else {
            throw ApiError.requestFailed(description: "invalid response")
        }
        guard response.statusCode == 200 else {
            throw ApiError.responseUnsuccessful(description: "status code: \(response.statusCode)")
        }
    }
}
