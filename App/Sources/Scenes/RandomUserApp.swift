//
//  RandomUserApp.swift
//  RandomUser
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 tuist.io. All rights reserved.
//

import SwiftUI

@main
struct RandomUserApp: App {

    var body: some Scene {
        #if os(iOS)
        IOSScene()
        #endif
    }
}
