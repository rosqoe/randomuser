//
//  CallToActionButton.swift
//  App
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import SwiftUI

struct CallToActionButton: View {

    // MARK: - Properties

    let label: String
    let action: () -> Void

    // MARK: - Core View

    var body: some View {
        Button(action: action) {
            Text(label)
                .foregroundColor(Color.white)
                .font(.title)
                .fontWeight(.semibold)
                .padding()
                .background(Color(.systemTeal))
                .cornerRadius(10)
        }
    }
}

#if DEBUG
struct CallToActionButton_Previews: PreviewProvider {
    static var previews: some View {
        CallToActionButton(label: "Refresh", action: {})
            .previewLayout(.sizeThatFits)
    }
}
#endif
