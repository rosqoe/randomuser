//
//  InformationLine.swift
//  App
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import SwiftUI

struct InformationLine: View {

    // MARK: - Properties

    let icon: String
    let info: String

    // MARK: - Core View

    var body: some View {
        HStack {
            Image(systemName: icon)
            Text(info)
                .fontWeight(.semibold)
            Spacer()
        }
        .font(.title3)
    }
}

#if DEBUG
struct InformationLine_Previews: PreviewProvider {
    static var previews: some View {
        InformationLine(icon: "person.fill", info: "Eden Moore")
            .previewLayout(.sizeThatFits)
    }
}
#endif
