//
//  Thumbnail.swift
//  App
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import SwiftUI

struct Thumbnail: View {

    // MARK: - Properties

    let pictureUrl: String

    // MARK: - Core View

    var body: some View {
        AsyncImage(url: .init(string: pictureUrl), content: { image in
            image
                .resizable()
                .scaledToFit()
        }, placeholder: {
            ProgressView()
        })
        .clipShape(Circle())
        .overlay(Circle().stroke(Color.white, lineWidth: 4))
        .shadow(radius: 7)
    }
}

#if DEBUG
struct Thumbnail_Previews: PreviewProvider {
    static var previews: some View {
        Thumbnail(pictureUrl: "https://randomuser.me/api/portraits/women/34.jpg")
            .previewLayout(.sizeThatFits)
    }
}
#endif
