//
//  UserInfoContainerView.swift
//  RandomUser
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 tuist.io. All rights reserved.
//

import SwiftUI

struct UserInfoContainerView: View {

    // MARK: - Properties

    @ObservedObject var viewModel: UserInfoViewModel

    // MARK: - Core View

    var body: some View {
        VStack {
            Color(.systemTeal)
                .edgesIgnoringSafeArea(.top)
                .frame(height: 200)
            Thumbnail(pictureUrl: viewModel.output.imageUrl)
                .frame(width: 150)
                .offset(y: -75)
                .padding(.bottom, -75)
            informationList
                .padding(.vertical, 30)
                .padding(.horizontal, 20)
            Spacer()
            CallToActionButton(label: "Refresh", action: refreshAction)
                .padding(.bottom, 20)
        }
        .alert(isPresented: .constant($viewModel.error.wrappedValue != nil)) { alert }
    }

    // MARK: - Views

    var informationList: some View {
        VStack(spacing: 20) {
            ForEach(viewModel.output.lines, id: \.self) { line in
                InformationLine(icon: line.icon, info: line.info)
            }
        }
    }

    var alert: Alert {
        Alert(title: .init("Error"),
              message: .init(viewModel.error!.description),
              dismissButton: .default(.init("Ok"),
                                      action: { viewModel.error = nil })
        )
    }

    // MARK: - Actions

    private func refreshAction() {
        viewModel.loadNewUser()
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        UserInfoContainerView(viewModel: .init())
    }
}
#endif
