//
//  UserInfoViewModel.swift
//  App
//
//  Created by Sebastien Bastide on 25/10/2021.
//  Copyright © 2021 rosqoe. All rights reserved.
//

import Foundation
import SwiftUI

final class UserInfoViewModel: ObservableObject {

    // MARK: - Properties

    @Published var output: Output = UserInfoViewModel.initialOutput
    @Published var error: ApiError?

    private let loaderUserUseCase: LoaderUserUseCaseLogic

    // MARK: - Initializer

    init(loaderUserUseCase: LoaderUserUseCaseLogic = LoaderUserUseCase()) {
        self.loaderUserUseCase = loaderUserUseCase
    }

    // MARK: - Output

    struct Output {
        let imageUrl: String
        var lines: [Line]
    }

    // MARK: - Actions

    func loadNewUser() {
        Task {
            do {
                let user = try await loaderUserUseCase.load().results[0]
                await MainActor.run { self.output = generateOutput(from: user) }
            } catch let error as ApiError {
                await MainActor.run { self.error = error }
            }
        }
    }

    // MARK: - Helpers

    private func generateOutput(from user: UserApiResponse.Result) -> Output {
        let lines: [Output.Line] = [
            .init(icon: "person.fill", info: "\(user.first) \(user.last)"),
            .init(icon: "lightbulb.fill", info: "\(user.age)"),
            .init(icon: "envelope.fill", info: user.email),
            .init(icon: "phone.fill", info: user.phone),
            .init(icon: "house.fill", info: user.city)
         ]
        return .init(imageUrl: user.imageUrl, lines: lines)
    }
}

extension UserInfoViewModel.Output {
    struct Line: Hashable {
        let icon: String
        let info: String
    }
}

extension UserInfoViewModel {
    static let initialOutput: Output = .init(imageUrl: "https://randomuser.me/api/portraits/women/34.jpg",
                                             lines: [
        .init(icon: "person.fill", info: "Eden Moore"),
        .init(icon: "lightbulb.fill", info: "71"),
        .init(icon: "envelope.fill", info: "eden.moore@example.com"),
        .init(icon: "phone.fill", info: "(011)-816-3274"),
        .init(icon: "house.fill", info: "Taupo")
    ])
}
