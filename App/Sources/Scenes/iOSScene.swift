//
//  iOSScene.swift
//  RandomUser
//
//  Created by Sebastien Bastide on 24/10/2021.
//  Copyright © 2021 tuist.io. All rights reserved.
//

import SwiftUI

struct IOSScene: Scene {

    var body: some Scene {
        WindowGroup {
            UserInfoContainerView(viewModel: .init())
        }
    }
}
