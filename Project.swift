import ProjectDescription

let targetActions = [
    TargetAction.pre(path: "Scripts/SwiftLintRunScript.sh",
                     arguments: [],
                     name: "SwiftLint")
]

let infoPlist: [String: InfoPlist.Value] = [
    "UILaunchScreen": [:],
    "UISupportedInterfaceOrientations": ["UIInterfaceOrientationPortrait"]
]

let targets: [Target] = [
    .init(name: "App",
          platform: .iOS,
          product: .app,
          bundleId: "com.rosqoe.app",
          deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
          infoPlist: .extendingDefault(with: infoPlist),
          sources: ["App/Sources/**", "App/Helpers/**"],
          resources: ["App/Resources/**"],
          actions: targetActions,
          dependencies: [.cocoapods(path: ".")]),
    .init(name: "AppTests",
          platform: .iOS,
          product: .unitTests,
          bundleId: "com.rosqoe.apptests",
          deploymentTarget: .iOS(targetVersion: "15.0", devices: .ipad),
          infoPlist: .default,
          sources: ["AppTests/Sources/**"],
          dependencies: [.target(name: "App")])
]

let project: Project = .init(name: "RandomUser",
                             organizationName: "rosqoe",
                             targets: targets)
